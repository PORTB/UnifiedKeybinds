/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.mixin;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import manifold.rt.api.NoBootstrap;
import net.minecraft.client.AbstractOption;
import net.minecraft.client.GameSettings;
import net.minecraft.client.renderer.VideoMode;
import net.minecraft.client.settings.*;
import net.minecraft.client.tutorial.TutorialSteps;
import net.minecraft.entity.player.ChatVisibility;
import net.minecraft.entity.player.PlayerModelPart;
import net.minecraft.util.HandSide;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.Difficulty;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import portb.unifiedoptions.pre17.OptionValue;
import portb.unifiedoptions.pre17.OptionsSerializer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@NoBootstrap
@Mixin(GameSettings.class)
public class GameSettingsMixin
{
    private static final List<OptionValue> SETTINGS_LIST = new ArrayList<>();
    
    private static void addOption(String name, OptionValue.Func2Params<GameSettings, String> read, Function<GameSettings, String> save)
    {
        SETTINGS_LIST.add(new OptionValue(name, read, save));
    }
    
    static
    {
        Gson gson = new Gson();
        
        addOption("autoJump",
                  AbstractOption.AUTO_JUMP::set,
                  (settings) -> String.valueOf(AbstractOption.AUTO_JUMP.get(settings))
        );
        addOption("autoSuggestions",
                  AbstractOption.AUTO_SUGGESTIONS::set,
                  (settings) -> String.valueOf(AbstractOption.AUTO_SUGGESTIONS.get(settings))
        );
        addOption("chatColors",
                  AbstractOption.CHAT_COLOR::set,
                  (settings) -> String.valueOf(AbstractOption.CHAT_COLOR.get(settings))
        );
        addOption("chatLinks",
                  AbstractOption.CHAT_LINKS::set,
                  (settings) -> String.valueOf(AbstractOption.CHAT_LINKS.get(settings))
        );
        addOption("chatLinksPrompt",
                  AbstractOption.CHAT_LINKS_PROMPT::set,
                  (settings) -> String.valueOf(AbstractOption.CHAT_LINKS_PROMPT.get(settings))
        );
        addOption("enableVsync",
                  AbstractOption.ENABLE_VSYNC::set,
                  (settings) -> String.valueOf(AbstractOption.ENABLE_VSYNC.get(settings))
        );
        addOption("entityShadows",
                  AbstractOption.ENTITY_SHADOWS::set,
                  (settings) -> String.valueOf(AbstractOption.ENTITY_SHADOWS.get(settings))
        );
        addOption("forceUnicodeFont",
                  AbstractOption.FORCE_UNICODE_FONT::set,
                  (settings) -> String.valueOf(AbstractOption.FORCE_UNICODE_FONT.get(settings))
        );
        addOption("discrete_mouse_scroll",
                  AbstractOption.DISCRETE_MOUSE_SCROLL::set,
                  (settings) -> String.valueOf(AbstractOption.DISCRETE_MOUSE_SCROLL.get(settings))
        );
        addOption("invertYMouse",
                  AbstractOption.INVERT_MOUSE::set,
                  (settings) -> String.valueOf(AbstractOption.INVERT_MOUSE.get(settings))
        );
        addOption("realmsNotifications",
                  AbstractOption.REALMS_NOTIFICATIONS::set,
                  (settings) -> String.valueOf(AbstractOption.REALMS_NOTIFICATIONS.get(settings))
        );
        addOption("reducedDebugInfo",
                  AbstractOption.REDUCED_DEBUG_INFO::set,
                  (settings) -> String.valueOf(AbstractOption.REDUCED_DEBUG_INFO.get(settings))
        );
        addOption("snooperEnabled",
                  AbstractOption.SNOOPER_ENABLED::set,
                  (settings) -> String.valueOf(AbstractOption.SNOOPER_ENABLED.get(settings))
        );
        addOption("showSubtitles",
                  AbstractOption.SHOW_SUBTITLES::set,
                  (settings) -> String.valueOf(AbstractOption.SHOW_SUBTITLES.get(settings))
        );
        addOption("touchscreen",
                  AbstractOption.TOUCHSCREEN::set,
                  (settings) -> String.valueOf(AbstractOption.TOUCHSCREEN.get(settings))
        );
        addOption("fullscreen",
                  AbstractOption.USE_FULLSCREEN::set,
                  (settings) -> String.valueOf(AbstractOption.USE_FULLSCREEN.get(settings))
        );
        addOption("bobView",
                  AbstractOption.VIEW_BOBBING::set,
                  (settings) -> String.valueOf(AbstractOption.VIEW_BOBBING.get(settings))
        );
        addOption("toggleCrouch",
                  (settings, str) -> settings.toggleCrouch = "true".equals(str),
                  (settings) -> String.valueOf(settings.toggleCrouch)
        );
        addOption("toggleSprint",
                  (settings, str) -> settings.toggleSprint = "true".equals(str),
                  (settings) -> String.valueOf(settings.toggleSprint)
        );
        addOption("mouseSensitivity",
                  (settings, str) -> settings.sensitivity = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.sensitivity)
        );
        addOption("fov",
                  (settings, str) -> settings.fov = (GameSettings.readFloat(str) * 40.0F + 70.0F),
                  (settings) -> String.valueOf((settings.fov - 70.0D) / 40.0D)
        );
        addOption("screenEffectScale",
                  (settings, str) -> settings.screenEffectScale = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.screenEffectScale)
        );
        addOption("fovEffectScale",
                  (settings, str) -> settings.fovEffectScale = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.fovEffectScale)
        );
        addOption("gamma",
                  (settings, str) -> settings.gamma = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.gamma)
        );
        addOption("renderDistance",
                  (settings, str) -> settings.renderDistance = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.renderDistance)
        );
        addOption("entityDistanceScaling",
                  (settings, str) -> settings.entityDistanceScaling = Float.parseFloat(str),
                  (settings) -> String.valueOf(settings.entityDistanceScaling)
        );
        addOption("guiScale",
                  (settings, str) -> settings.guiScale = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.guiScale)
        );
        addOption("particles",
                  (settings, str) -> settings.particles = ParticleStatus.byId(Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.particles.getId())
        );
        addOption("maxFps",
                  (settings, str) -> {
                      settings.framerateLimit = Integer.parseInt(str);
                      if (settings.minecraft.getWindow() != null)
                      {
                          settings.minecraft.getWindow().setFramerateLimit(settings.framerateLimit);
                      }
                  },
                  (settings) -> String.valueOf(settings.framerateLimit)
        );
        addOption("difficulty",
                  (settings, str) -> settings.difficulty = Difficulty.byId(Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.difficulty.getId())
        );
        addOption("graphicsMode",
                  (settings, str) -> settings.graphicsMode = GraphicsFanciness.byId(Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.graphicsMode.getId())
        );
        addOption("ao",
                  (settings, str) ->
                  {
                      if ("true".equals(str))
                      {
                          settings.ambientOcclusion = AmbientOcclusionStatus.MAX;
                      }
                      else if ("false".equals(str))
                      {
                          settings.ambientOcclusion = AmbientOcclusionStatus.OFF;
                      }
                      else
                      {
                          settings.ambientOcclusion = AmbientOcclusionStatus.byId(Integer.parseInt(str));
                      }
                  },
                  (settings) -> String.valueOf(settings.ambientOcclusion.getId())
        );
        addOption("biomeBlendRadius",
                  (settings, str) -> settings.biomeBlendRadius = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.biomeBlendRadius)
        );
        addOption("resourcePacks",
                  (settings, str) -> {
                      settings.resourcePacks = JSONUtils.fromJson(gson, str, GameSettings.RESOURCE_PACK_TYPE);
            
                      if (settings.resourcePacks == null)
                      {
                          settings.resourcePacks = Lists.newArrayList();
                      }
                  },
                  (settings) -> String.valueOf(gson.toJson(settings.resourcePacks))
        );
        addOption("incompatibleResourcePacks",
                  (settings, str) ->
                  {
                      settings.incompatibleResourcePacks = JSONUtils.fromJson(gson,
                                                                              str,
                                                                              GameSettings.RESOURCE_PACK_TYPE
                      );
                      if (settings.incompatibleResourcePacks == null)
                      {
                          settings.incompatibleResourcePacks = Lists.newArrayList();
                      }
                  },
                  (settings) -> String.valueOf(gson.toJson(settings.incompatibleResourcePacks))
        );
        addOption("lastServer",
                  (settings, str) -> settings.lastMpIp = str,
                  (settings) -> String.valueOf(settings.lastMpIp)
        );
        addOption("lang",
                  (settings, str) -> settings.languageCode = str,
                  (settings) -> String.valueOf(settings.languageCode)
        );
        addOption("chatVisibility",
                  (settings, str) -> settings.chatVisibility = ChatVisibility.byId(Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.chatVisibility.getId())
        );
        addOption("chatOpacity",
                  (settings, str) -> settings.chatOpacity = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.chatOpacity)
        );
        addOption("chatLineSpacing",
                  (settings, str) -> settings.chatLineSpacing = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.chatLineSpacing)
        );
        addOption("textBackgroundOpacity",
                  (settings, str) -> settings.textBackgroundOpacity = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.textBackgroundOpacity)
        );
        addOption("backgroundForChatOnly",
                  (settings, str) -> settings.backgroundForChatOnly = "true".equals(str),
                  (settings) -> String.valueOf(settings.backgroundForChatOnly)
        );
        addOption("fullscreenResolution",
                  (settings, str) -> settings.fullscreenVideoModeString = str,
                  (settings) -> settings.minecraft.getWindow().getPreferredFullscreenVideoMode().map(VideoMode::write).orElse(null)
        );
        addOption("hideServerAddress",
                  (settings, str) -> settings.hideServerAddress = "true".equals(str),
                  (settings) -> String.valueOf(settings.hideServerAddress)
        );
        addOption("advancedItemTooltips",
                  (settings, str) -> settings.advancedItemTooltips = "true".equals(str),
                  (settings) -> String.valueOf(settings.advancedItemTooltips)
        );
        addOption("pauseOnLostFocus",
                  (settings, str) -> settings.pauseOnLostFocus = "true".equals(str),
                  (settings) -> String.valueOf(settings.pauseOnLostFocus)
        );
        addOption("overrideWidth",
                  (settings, str) -> settings.overrideWidth = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.overrideWidth)
        );
        addOption("overrideHeight",
                  (settings, str) -> settings.overrideHeight = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.overrideHeight)
        );
        addOption("heldItemTooltips",
                  (settings, str) -> settings.heldItemTooltips = "true".equals(str),
                  (settings) -> String.valueOf(settings.heldItemTooltips)
        );
        addOption("chatHeightFocused",
                  (settings, str) -> settings.chatHeightFocused = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.chatHeightFocused)
        );
        addOption("chatDelay",
                  (settings, str) -> settings.chatDelay = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.chatDelay)
        );
        addOption("chatHeightUnfocused",
                  (settings, str) -> settings.chatHeightUnfocused = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.chatHeightUnfocused)
        );
        addOption("chatScale",
                  (settings, str) -> settings.chatScale = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.chatScale)
        );
        addOption("chatWidth",
                  (settings, str) -> settings.chatWidth = GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.chatWidth)
        );
        addOption("mipmapLevels",
                  (settings, str) -> settings.mipmapLevels = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.mipmapLevels)
        );
        addOption("useNativeTransport",
                  (settings, str) -> settings.useNativeTransport = "true".equals(str),
                  (settings) -> String.valueOf(settings.useNativeTransport)
        );
        addOption("mainHand",
                  (settings, str) -> settings.mainHand = "left".equals(str) ? HandSide.LEFT : HandSide.RIGHT,
                  (settings) -> settings.mainHand == HandSide.LEFT ? "left" : "right"
        );
        addOption("attackIndicator",
                  (settings, str) -> settings.attackIndicator = AttackIndicatorStatus.byId(Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.attackIndicator.getId())
        );
        addOption("narrator",
                  (settings, str) -> settings.narratorStatus = NarratorStatus.byId(Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.narratorStatus.getId())
        );
        //tutorial has been disabled
//        addOption("tutorialStep",
//                  (settings, str) -> settings.tutorialStep = TutorialSteps.getByName(str),
//                  (settings) -> settings.tutorialStep.getName()
//        );
        addOption("mouseWheelSensitivity",
                  (settings, str) -> settings.mouseWheelSensitivity = (double) GameSettings.readFloat(str),
                  (settings) -> String.valueOf(settings.mouseWheelSensitivity)
        );
        addOption("rawMouseInput",
                  (settings, str) -> settings.rawMouseInput = "true".equals(str),
                  (settings) -> String.valueOf(AbstractOption.RAW_MOUSE_INPUT.get(settings))
        );
        addOption("glDebugVerbosity",
                  (settings, str) -> settings.glDebugVerbosity = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.glDebugVerbosity)
        );
        addOption("skipMultiplayerWarning",
                  (settings, str) -> settings.skipMultiplayerWarning = "true".equals(str),
                  (settings) -> String.valueOf(settings.skipMultiplayerWarning)
        );
        addOption("hideMatchedNames",
                  (settings, str) -> settings.hideMatchedNames = "true".equals(str),
                  (settings) -> String.valueOf(settings.hideMatchedNames)
        );
        addOption("joinedFirstServer",
                  (settings, str) -> settings.joinedFirstServer = "true".equals(str),
                  (settings) -> String.valueOf(settings.joinedFirstServer)
        );
        addOption("syncChunkWrites",
                  (settings, str) -> settings.syncWrites = "true".equals(str),
                  (settings) -> String.valueOf(settings.syncWrites)
        );
        addOption("renderClouds",
                  (settings, str) ->
                  {
                      if ("true".equals(str))
                      {
                          settings.renderClouds = CloudOption.FANCY;
                      }
                      else if ("false".equals(str))
                      {
                          settings.renderClouds = CloudOption.OFF;
                      }
                      else if ("fast".equals(str))
                      {
                          settings.renderClouds = CloudOption.FAST;
                      }
                  },
                  (settings) -> {
                      switch (settings.renderClouds)
                      {
                          case FANCY:
                              return "true";
                          case FAST:
                              return "fast";
                          default:
                              return "false";
                      }
                  }
        );
        
        for (SoundCategory soundcategory : SoundCategory.values())
        {
            SETTINGS_LIST.add(serializeSoundCategory(soundcategory));
        }
    
        for (PlayerModelPart enumPlayerModelParts : PlayerModelPart.values())
        {
            SETTINGS_LIST.add(serializeSkinOptions(enumPlayerModelParts));
        }
    }
    
    private static OptionValue serializeSoundCategory(SoundCategory soundCategory)
    {
        return new OptionValue(
                "soundCategory_" + soundCategory.name,
                (settings, s) -> settings.sourceVolumes.put(soundCategory, GameSettings.readFloat(s)),
                (gameSettings) -> String.valueOf(gameSettings.getSoundSourceVolume(soundCategory))
        );
    }
    
    private static OptionValue serializeSkinOptions(PlayerModelPart modelParts)
    {
        return new OptionValue(
                "modelPart_" + modelParts.id,
                (settings, s) -> settings.setModelPart(modelParts, "true".equals(s)),
                (gameSettings) -> String.valueOf(gameSettings.modelParts.contains(modelParts))
        );
    }
    
    /**
     * @author PORTB
     * @reason Completely redo how options are loaded. The vanilla method is very poorly done and is pretty useless as I can't track what options the game does not read.
     */
    @Overwrite
    public void load()
    {
        OptionsSerializer.load((GameSettings) (Object) this, SETTINGS_LIST);
    }
    
    /**
     * @author PORTB
     * @reason Completely redo how options are saved
     */
    @Overwrite
    public void save()
    {
        OptionsSerializer.save((GameSettings) (Object) this, SETTINGS_LIST);
    }
}
