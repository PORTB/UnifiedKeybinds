/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.mixin;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import manifold.rt.api.NoBootstrap;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.EnumDifficulty;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import portb.unifiedoptions.pre17.OptionValue;
import portb.unifiedoptions.pre17.OptionsSerializer;

import java.util.*;
import java.util.function.Function;

@NoBootstrap
@Mixin(GameSettings.class)
public class GamesSettingsMixin
{
    private static final List<OptionValue> SETTINGS_LIST = new ArrayList<>();
    
    private static void addOption(String name, OptionValue.Func2Params<GameSettings, String> read, Function<GameSettings, String> save)
    {
        SETTINGS_LIST.add(new OptionValue(name, read, save));
    }
    
    static
    {
        Gson gson = new Gson();
        
        addOption("invertYMouse",
                  (settings, str) -> settings.invertMouse = "true".equals(str),
                  (settings) -> String.valueOf(settings.invertMouse)
        );
        addOption("mouseSensitivity",
                  (settings, str) -> settings.mouseSensitivity = settings.parseFloat(str),
                  (settings) -> String.valueOf(settings.mouseSensitivity)
        );
        addOption("fov",
                  (settings, str) -> settings.fovSetting =
                                             settings.parseFloat(str) * 40.0F + 70.0F,
                  (settings) -> String.valueOf((settings.fovSetting - 70.0F) / 40.0F)
        );
        addOption("gamma",
                  (settings, str) -> settings.gammaSetting = settings.parseFloat(str),
                  (settings) -> String.valueOf(settings.gammaSetting)
        );
        addOption("saturation",
                  (settings, str) -> settings.saturation = settings.parseFloat(str),
                  (settings) -> String.valueOf(settings.saturation)
        );
        addOption("renderDistance",
                  (settings, str) -> settings.renderDistanceChunks = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.renderDistanceChunks)
        );
        addOption("guiScale",
                  (settings, str) -> settings.guiScale = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.guiScale)
        );
        addOption("particles",
                  (settings, str) -> settings.particleSetting = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.particleSetting)
        );
        addOption("bobView",
                  (settings, str) -> settings.viewBobbing = "true".equals(str),
                  (settings) -> String.valueOf(settings.viewBobbing)
        );
        addOption("anaglyph3d",
                  (settings, str) -> settings.anaglyph = "true".equals(str),
                  (settings) -> String.valueOf(settings.anaglyph)
        );
        addOption("maxFps",
                  (settings, str) -> settings.limitFramerate = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.limitFramerate)
        );
        addOption("fboEnable",
                  (settings, str) -> settings.fboEnable = "true".equals(str),
                  (settings) -> String.valueOf(settings.fboEnable)
        );
        addOption("difficulty",
                  (settings, str) -> settings.difficulty = EnumDifficulty.getDifficultyEnum(
                          Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.difficulty.getDifficultyId())
        );
        addOption("fancyGraphics",
                  (settings, str) -> settings.fancyGraphics = "true".equals(str),
                  (settings) -> String.valueOf(settings.fancyGraphics)
        );
        addOption("ao",
                  (settings, str) -> {
                      if ("true".equals(str))
                          settings.ambientOcclusion = 2;
                      else if ("false".equals(str))
                          settings.ambientOcclusion = 0;
                      else
                          settings.ambientOcclusion = Integer.parseInt(str);
                  },
                  (settings) -> String.valueOf(settings.ambientOcclusion)
        );
        addOption("renderClouds",
                  (settings, str) -> {
                      if ("true".equals(str))
                          settings.clouds = 2;
                      else if ("false".equals(str))
                          settings.clouds = 0;
                      else if ("fast".equals(str))
                          settings.clouds = 1;
                  },
                  settings -> {
                      switch (settings.clouds)
                      {
                          case 0:
                          default:
                              return "false";
                          case 1:
                              return "fast";
                          case 2:
                              return "true";
                      }
                  }
        );
        addOption("resourcePacks",
                  (settings, str) -> {
                      settings.resourcePacks = JsonUtils.gsonDeserialize(gson, str, GameSettings.TYPE_LIST_STRING);
            
                      if (settings.resourcePacks == null)
                          settings.resourcePacks = Lists.newArrayList();
                  },
                  (settings) -> gson.toJson(settings.resourcePacks)
        );
        addOption("incompatibleResourcePacks",
                  (settings, str) -> {
                      settings.incompatibleResourcePacks = JsonUtils.gsonDeserialize(gson,
                                                                                     str,
                                                                                     GameSettings.TYPE_LIST_STRING
                      );
            
                      if (settings.incompatibleResourcePacks == null)
                          settings.incompatibleResourcePacks = Lists.newArrayList();
                  },
                  (settings) -> gson.toJson(settings.incompatibleResourcePacks)
        );
        addOption("lastServer",
                  (settings, str) -> settings.lastServer = str,
                  (settings) -> String.valueOf(settings.lastServer)
        );
        addOption("lang",
                  (settings, str) -> settings.language = str,
                  (settings) -> String.valueOf(settings.language)
        );
        addOption("chatVisibility",
                  (settings, str) -> settings.chatVisibility = EntityPlayer.EnumChatVisibility.getEnumChatVisibility(
                          Integer.parseInt(str)),
                  (settings) -> String.valueOf(settings.chatVisibility.getChatVisibility())
        );
        addOption("chatColors",
                  (settings, str) -> settings.chatColours = "true".equals(str),
                  (settings) -> String.valueOf(settings.chatColours)
        );
        addOption("chatLinks",
                  (settings, str) -> settings.chatLinks = "true".equals(str),
                  (settings) -> String.valueOf(settings.chatLinks)
        );
        addOption("chatLinksPrompt",
                  (settings, str) -> settings.chatLinksPrompt = "true".equals(str),
                  (settings) -> String.valueOf(settings.chatLinksPrompt)
        );
        addOption("chatOpacity",
                  (settings, str) -> settings.chatOpacity = settings.parseFloat(str),
                  (settings) -> String.valueOf(settings.chatOpacity)
        );
        addOption("snooperEnabled",
                  (settings, str) -> settings.snooperEnabled = "true".equals(str),
                  (settings) -> String.valueOf(settings.snooperEnabled)
        );
        addOption("fullscreen",
                  (settings, str) -> settings.fullScreen = "true".equals(str),
                  (settings) -> String.valueOf(settings.fullScreen)
        );
        addOption("enableVsync",
                  (settings, str) -> settings.enableVsync = "true".equals(str),
                  (settings) -> String.valueOf(settings.enableVsync)
        );
        addOption("useVbo",
                  (settings, str) -> settings.useVbo = "true".equals(str),
                  (settings) -> String.valueOf(settings.useVbo)
        );
        addOption("hideServerAddress",
                  (settings, str) -> settings.hideServerAddress = "true".equals(str),
                  (settings) -> String.valueOf(settings.hideServerAddress)
        );
        addOption("advancedItemTooltips",
                  (settings, str) -> settings.advancedItemTooltips = "true".equals(str),
                  (settings) -> String.valueOf(settings.advancedItemTooltips)
        );
        addOption("pauseOnLostFocus",
                  (settings, str) -> settings.pauseOnLostFocus = "true".equals(str),
                  (settings) -> String.valueOf(settings.pauseOnLostFocus)
        );
        addOption("touchscreen",
                  (settings, str) -> settings.touchscreen = "true".equals(str),
                  (settings) -> String.valueOf(settings.touchscreen)
        );
        addOption("overrideWidth",
                  (settings, str) -> settings.overrideWidth = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.overrideWidth)
        );
        addOption("overrideHeight",
                  (settings, str) -> settings.overrideHeight = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.overrideHeight)
        );
        addOption("heldItemTooltips",
                  (settings, str) -> settings.heldItemTooltips = "true".equals(str),
                  (settings) -> String.valueOf(settings.heldItemTooltips)
        );
        addOption("chatHeightFocused",
                  (settings, str) -> settings.chatHeightFocused = settings.parseFloat(str),
                  (settings) -> String.valueOf(settings.chatHeightFocused)
        );
        addOption("chatHeightUnfocused",
                  (settings, str) -> settings.chatHeightUnfocused = settings.parseFloat(
                          str), (settings) -> String.valueOf(settings.chatHeightUnfocused)
        );
        addOption("chatScale",
                  (settings, str) -> settings.chatScale = settings.parseFloat(str),
                  (settings) -> String.valueOf(settings.chatScale)
        );
        addOption("chatWidth",
                  (settings, str) -> settings.chatWidth = settings.parseFloat(str),
                  (settings) -> String.valueOf(settings.chatWidth)
        );
        addOption("mipmapLevels",
                  (settings, str) -> settings.mipmapLevels = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.mipmapLevels)
        );
        addOption("forceUnicodeFont",
                  (settings, str) -> settings.forceUnicodeFont = "true".equals(str),
                  (settings) -> String.valueOf(settings.forceUnicodeFont)
        );
        addOption("reducedDebugInfo",
                  (settings, str) -> settings.reducedDebugInfo = "true".equals(str),
                  (settings) -> String.valueOf(settings.reducedDebugInfo)
        );
        addOption("useNativeTransport",
                  (settings, str) -> settings.useNativeTransport = "true".equals(str),
                  (settings) -> String.valueOf(settings.useNativeTransport)
        );
        addOption("entityShadows",
                  (settings, str) -> settings.entityShadows = "true".equals(str),
                  (settings) -> String.valueOf(settings.entityShadows)
        );
        addOption("mainHand",
                  (settings, str) -> settings.mainHand = "left".equals(str) ? EnumHandSide.LEFT : EnumHandSide.RIGHT,
                  (settings) -> settings.mainHand == EnumHandSide.LEFT ? "left" : "right"
        );
        addOption("attackIndicator",
                  (settings, s) -> settings.attackIndicator = Math.max(0, Math.min(2, Integer.parseInt(s))),
                  (settings) -> String.valueOf(settings.attackIndicator)
        );
        addOption("showSubtitles",
                  (settings, str) -> settings.showSubtitles = "true".equals(str),
                  (settings) -> String.valueOf(settings.showSubtitles)
        );
        addOption("realmsNotifications",
                  (settings, str) -> settings.realmsNotifications = "true".equals(str),
                  (settings) -> String.valueOf(settings.realmsNotifications)
        );
        addOption("enableWeakAttacks",
                  (settings, str) -> settings.enableWeakAttacks = "true".equals(str),
                  (settings) -> String.valueOf(settings.enableWeakAttacks)
        );
        addOption("autoJump",
                  (settings, str) -> settings.autoJump = "true".equals(str),
                  (settings) -> String.valueOf(settings.autoJump)
        );
        addOption("narrator",
                  (settings, str) -> settings.narrator = Integer.parseInt(str),
                  (settings) -> String.valueOf(settings.narrator)
        );
        //tutorial has been disabled
//        addOption("tutorialStep",
//                  (settings, str) -> settings.tutorialStep = TutorialSteps.getTutorial(str),
//                  (settings) -> settings.tutorialStep.getName()
//        );
        
        for (SoundCategory soundcategory : SoundCategory.values())
        {
            SETTINGS_LIST.add(serializeSoundCategory(soundcategory));
        }
        
        for (EnumPlayerModelParts enumPlayerModelParts : EnumPlayerModelParts.values())
        {
            SETTINGS_LIST.add(serializeSkinOptions(enumPlayerModelParts));
        }
    }
    
    private static OptionValue serializeSoundCategory(SoundCategory soundCategory)
    {
        return new OptionValue(
                "soundCategory_" + soundCategory.name,
                (settings, s) -> settings.soundLevels.put(soundCategory, settings.parseFloat(s)),
                (gameSettings) -> String.valueOf(gameSettings.getSoundLevel(soundCategory))
        );
    }
    
    private static OptionValue serializeSkinOptions(EnumPlayerModelParts modelParts)
    {
        return new OptionValue(
                "modelPart_" + modelParts.partName,
                (settings, s) -> settings.setModelPartEnabled(modelParts, "true".equals(s)),
                (gameSettings) -> String.valueOf(gameSettings.modelParts.contains(modelParts))
        );
    }
    
    /**
     * @author PORTB
     * @reason Completely redo how options are loaded. The vanilla method is very poorly done and is pretty useless as I can't track what options the game does not read.
     */
    @Overwrite
    public void loadOptions()
    {
        OptionsSerializer.load((GameSettings) (Object) this, SETTINGS_LIST);
    }
    
    /**
     * @author PORTB
     * @reason Completely redo how options are saved
     */
    @Overwrite
    public void saveOptions()
    {
        OptionsSerializer.save((GameSettings) (Object) this, SETTINGS_LIST);
    }
}
