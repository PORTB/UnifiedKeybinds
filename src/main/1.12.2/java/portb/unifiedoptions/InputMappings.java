/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions;

import manifold.rt.api.NoBootstrap;
import org.lwjgl.Sys;

import java.util.HashMap;
import java.util.Map;

@NoBootstrap
public class InputMappings
{
    private static final Map<String, Integer> NAME_TO_KEYCODE_MAP = new HashMap<>();
    private static final Map<Integer, String> KEYCODE_TO_NAME_MAP = new HashMap<>();
    
    private static void addKey(String name, int code)
    {
        NAME_TO_KEYCODE_MAP.put(name, code);
        KEYCODE_TO_NAME_MAP.put(code, name);
    }
    
    static
    {
        addKey("key.keyboard.0", 11);
        addKey("key.keyboard.1", 2);
        addKey("key.keyboard.2", 3);
        addKey("key.keyboard.3", 4);
        addKey("key.keyboard.4", 5);
        addKey("key.keyboard.5", 6);
        addKey("key.keyboard.6", 7);
        addKey("key.keyboard.7", 8);
        addKey("key.keyboard.8", 9);
        addKey("key.keyboard.9", 10);
        addKey("key.keyboard.a", 30);
        addKey("key.keyboard.b", 48);
        addKey("key.keyboard.c", 46);
        addKey("key.keyboard.d", 32);
        addKey("key.keyboard.e", 18);
        addKey("key.keyboard.f", 33);
        addKey("key.keyboard.g", 34);
        addKey("key.keyboard.h", 35);
        addKey("key.keyboard.i", 23);
        addKey("key.keyboard.j", 36);
        addKey("key.keyboard.k", 37);
        addKey("key.keyboard.l", 38);
        addKey("key.keyboard.m", 50);
        addKey("key.keyboard.n", 49);
        addKey("key.keyboard.o", 24);
        addKey("key.keyboard.p", 25);
        addKey("key.keyboard.q", 16);
        addKey("key.keyboard.r", 19);
        addKey("key.keyboard.s", 31);
        addKey("key.keyboard.t", 20);
        addKey("key.keyboard.u", 22);
        addKey("key.keyboard.v", 47);
        addKey("key.keyboard.w", 17);
        addKey("key.keyboard.x", 45);
        addKey("key.keyboard.y", 21);
        addKey("key.keyboard.z", 44);
        addKey("key.keyboard.f1", 59);
        addKey("key.keyboard.f2", 60);
        addKey("key.keyboard.f3", 61);
        addKey("key.keyboard.f4", 62);
        addKey("key.keyboard.f5", 63);
        addKey("key.keyboard.f6", 64);
        addKey("key.keyboard.f7", 65);
        addKey("key.keyboard.f8", 66);
        addKey("key.keyboard.f9", 67);
        addKey("key.keyboard.f10", 68);
        addKey("key.keyboard.f11", 87);
        addKey("key.keyboard.f12", 88);
        addKey("key.keyboard.f13", 100);
        addKey("key.keyboard.f14", 101);
        addKey("key.keyboard.f15", 102);
        addKey("key.keyboard.f16", 103);
        addKey("key.keyboard.f17", 104);
        addKey("key.keyboard.f18", 105);
        addKey("key.keyboard.f19", 113);
        addKey("key.keyboard.keypad.0", 82);
        addKey("key.keyboard.keypad.1", 79);
        addKey("key.keyboard.keypad.2", 80);
        addKey("key.keyboard.keypad.3", 81);
        addKey("key.keyboard.keypad.4", 75);
        addKey("key.keyboard.keypad.5", 76);
        addKey("key.keyboard.keypad.6", 77);
        addKey("key.keyboard.keypad.7", 71);
        addKey("key.keyboard.keypad.8", 72);
        addKey("key.keyboard.keypad.9", 73);
        addKey("key.keyboard.down", 208);
        addKey("key.keyboard.left", 203);
        addKey("key.keyboard.right", 205);
        addKey("key.keyboard.up", 200);
        addKey("key.keyboard.apostrophe", 40);
        addKey("key.keyboard.backslash", 43);
        addKey("key.keyboard.comma", 51);
        addKey("key.keyboard.left.bracket", 26);
        addKey("key.keyboard.minus", 12);
        addKey("key.keyboard.period", 52);
        addKey("key.keyboard.right.bracket", 27);
        addKey("key.keyboard.semicolon", 39);
        addKey("key.keyboard.slash", 53);
        addKey("key.keyboard.space", 57);
        addKey("key.keyboard.tab", 15);
        addKey("key.keyboard.left.alt", 56);
        addKey("key.keyboard.left.control", 29);
        addKey("key.keyboard.left.shift", 42);
        addKey("key.keyboard.right.alt", 184);
        addKey("key.keyboard.right.control", 157);
        addKey("key.keyboard.right.shift", 54);
        addKey("key.keyboard.escape", 1);
        addKey("key.keyboard.delete", 211);
        addKey("key.keyboard.end", 207);
        addKey("key.keyboard.home", 199);
        addKey("key.keyboard.insert", 210);
        addKey("key.keyboard.pause", 197);
        addKey("key.mouse.left", -100);
        addKey("key.mouse.right", -99);
        addKey("key.mouse.middle", -98);
        addKey("key.keyboard.num.lock", 69);
        addKey("key.keyboard.keypad.add", 78);
        addKey("key.keyboard.keypad.decimal", 83);
        addKey("key.keyboard.keypad.enter", 156);
        addKey("key.keyboard.keypad.multiply", 55);
        addKey("key.keyboard.keypad.divide", 181);
        addKey("key.keyboard.keypad.subtract", 74);
        addKey("key.keyboard.keypad.equal", 141);
        addKey("key.keyboard.equal", 13);
        addKey("key.keyboard.grave.accent", 41);
        addKey("key.keyboard.left.win", 219);
        addKey("key.keyboard.right.win", 220);
        addKey("key.keyboard.enter", 28);
        addKey("key.keyboard.backspace", 14);
        addKey("key.keyboard.page.down", 209);
        addKey("key.keyboard.page.up", 201);
        addKey("key.keyboard.caps.lock", 58);
        addKey("key.keyboard.scroll.lock", 70);
        addKey("key.keyboard.menu", 221);
        addKey("key.keyboard.print.screen", 183);
        
        addKey("key.mouse.left", -100);
        addKey("key.mouse.right", -99);
        addKey("key.mouse.middle", -98);
        
        //Not sure if these are correct
        addKey("key.mouse.4", -97);
        addKey("key.mouse.5", -96);
        addKey("key.mouse.6", -95);
        addKey("key.mouse.7", -94);
        addKey("key.mouse.8", -93);
        
        addKey("key.keyboard.unknown", 0);
    }
    
    /**
     * Takes a named key or keycode as `key`.
     * Key will be parsed to an int. If this fails, it will be treated as a named key and be converted to its keycode.
     * @param key A number or named key
     * @return The keycode for the key
     */
    public static int getKey(String key)
    {
        try
        {
            return Integer.parseInt(key);
        }
        catch (NumberFormatException e)
        {
            return NAME_TO_KEYCODE_MAP.get(key);
        }
        
    }
    
    public static String codeToNamedKey(int code)
    {
        return KEYCODE_TO_NAME_MAP.get(code);
    }
}
