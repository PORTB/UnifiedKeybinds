/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions;

import manifold.rt.api.NoBootstrap;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Set;

@NoBootstrap
public class MixinConfig implements IMixinConfigPlugin
{
    @Override
    public void onLoad(String mixinPackage)
    {
    
    }
    
    @Override
    public String getRefMapperConfig()
    {
        return null;
    }
    
    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName)
    {
        return Config.isEnabled;
    }
    
    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets)
    {
    
    }
    
    @Override
    public List<String> getMixins()
    {
        return null;
    }
    
    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo)
    {
    
    }
    
    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo)
    {
    
    }
}
