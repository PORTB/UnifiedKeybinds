/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.post17;

#if !(MC12 || MC16)
import manifold.rt.api.NoBootstrap;
import java.util.HashSet;
import java.util.Set;

/**
 * Tracks which options have been read by minecraft
 * {@link portb.unifiedoptions.mixin.OptionUseTracker}
 */
@NoBootstrap
public class UsedOptions
{
    public static Set<String> usedOptions = new HashSet<>();
}
#endif