/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.pre17;
#if MC12 || MC16

import manifold.ext.props.rt.api.var;
import manifold.rt.api.NoBootstrap;

import java.util.function.Function;

#if MC12
import net.minecraft.client.settings.GameSettings;
#else
import net.minecraft.client.GameSettings;
#endif

@NoBootstrap
public class OptionValue
{
    @var
    final String                         name;
    private final Function<GameSettings, String> save;
    private final Func2Params<GameSettings, String> read;
    
    public OptionValue(String name, Func2Params<GameSettings, String> read, Function<GameSettings, String> save)
    {
        this.name = name;
        this.save = save;
        this.read = read;
    }
    
    public String save(GameSettings settings)
    {
        return save.apply(settings);
    }
    
    public void read(GameSettings settings, String str)
    {
        read.apply(settings, str);
    }
    
    public interface Func2Params<A, B>
    {
        void apply(A a, B b);
    }
}
#endif
