/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.pre17;

#if MC12 || MC16

import manifold.ext.rt.api.auto;
import manifold.rt.api.NoBootstrap;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.client.settings.KeyModifier;
import portb.unifiedoptions.Config;
import portb.unifiedoptions.FileUtil;

#if MC12
import portb.unifiedoptions.InputMappings;
import net.minecraft.client.settings.GameSettings;
#else
import net.minecraft.client.util.InputMappings;
import net.minecraft.client.GameSettings;
#endif

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@NoBootstrap
public class OptionsSerializer
{
    private static String unusedOptions = "";
    
    public static void load(GameSettings gameSettings, List<OptionValue> settingsList)
    {
        Map<String, String> instanceOptions  = splitOptionsFileLinesIntoMap(FileUtil.readAllLinesOrEmptyList(gameSettings.optionsFile.toPath()));
        Map<String, String> universalOptions = splitOptionsFileLinesIntoMap(FileUtil.readAllLinesOrEmptyList(Config.universalOptionsFile));
        
        for(auto option : settingsList)
        {
            String value;
            
            if(Config.blacklistedUniversalOptions.contains(option.name))
            {
                if(!instanceOptions.containsKey(option.name))
                    continue;
                
                value = instanceOptions.get(option.name);
            }
            else
            {
                if(!universalOptions.containsKey(option.name))
                    continue;
                
                value = universalOptions.get(option.name);
                universalOptions.remove(option.name); //track what options have been used
            }
            
            option.read(gameSettings, value);
        }
        
        for(KeyBinding keyBinding : #if MC12 gameSettings.keyBindings #else gameSettings.keyMappings #endif)
        {
            
            auto keyBindingName = "key_" + #if MC12 keyBinding.keyDescription #else keyBinding.name #endif;
            String value;
            
            if(Config.blacklistedUniversalOptions.contains(keyBindingName))
            {
                if(!instanceOptions.containsKey(keyBindingName))
                    continue;
                
                value = instanceOptions.get(keyBindingName);
            }
            else
            {
                if(!universalOptions.containsKey(keyBindingName))
                    continue;
                
                value = universalOptions.get(keyBindingName);
                universalOptions.remove(keyBindingName); //track what options have been used
            }
            
            if (value.indexOf(':') != -1)
            {
                String[] split = value.split(":");
                keyBinding.setKeyModifierAndCode(KeyModifier.valueFromString(split[1]), InputMappings.getKey(split[0]));
            }
            else
            {
                keyBinding.setKeyModifierAndCode(KeyModifier.NONE, InputMappings.getKey(value));
            }
        }
        
        #if MC12
        KeyBinding.resetKeyBindingArrayAndHash();
        #else
        KeyBinding.resetMapping();
        #endif
        
        auto unusedOptionsBuilder = new StringBuilder();
        
        //preserve unused options
        for(auto option : universalOptions.entrySet())
        {
            unusedOptionsBuilder.append(option.key).append(":").append(option.value).append("\n");
        }
        
        unusedOptions = unusedOptionsBuilder.toString();
    }
    
    private static HashMap<String, String> splitOptionsFileLinesIntoMap(List<String> lines)
    {
        auto result = new HashMap<String, String>();
        
        for(String line : lines)
        {
            auto split = line.split(":", 2);
            
            if(split.length == 2)
                result.put(split[0], split[1]);
        }
        
        return result;
    }
    
    public static void save(GameSettings gameSettings, List<OptionValue> settingsList)
    {
        try(
                PrintWriter instance = new PrintWriter(new OutputStreamWriter(Files.newOutputStream(gameSettings.optionsFile.toPath()), StandardCharsets.UTF_8));
                PrintWriter universal = Config.universalOptionsPrintWriter
        )
        {
            for(auto entry : settingsList)
            {
                auto saveValue = entry.save(gameSettings);
                
                if(saveValue == null)
                    continue;
                
                if(Config.blacklistedUniversalOptions.contains(entry.name))
                    instance.println(entry.name + ":" + saveValue);
                else
                    universal.println(entry.name + ":" + saveValue);
            }
            
            for(KeyBinding keyBinding : #if MC12 gameSettings.keyBindings #else gameSettings.keyMappings #endif)
            {
                String name = "key_" + #if MC12 keyBinding.keyDescription #else keyBinding.name #endif;
                String value = name + ":" + getKeyName(keyBinding) + (keyBinding.getKeyModifier() != KeyModifier.NONE ? ":" + keyBinding.getKeyModifier() : "");
                
                if(Config.blacklistedUniversalOptions.contains(name))
                    instance.println(value);
                else
                    universal.println(value);
            }
            
            //preserve unused options
            universal.println(unusedOptions);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private static String getKeyName(KeyBinding keyBinding)
    {
        return #if MC12 InputMappings.codeToNamedKey(keyBinding.keyCode) #else keyBinding.saveString() #endif;
    }
}

#endif