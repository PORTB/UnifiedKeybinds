/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.mixin;

#if !(MC12 || MC16)

import manifold.rt.api.NoBootstrap;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import portb.unifiedoptions.post17.UsedOptions;

/**
 * Tracks what options are read by minecraft.
 * This is used to determine what options are *not* read and then append them to the end of the options file to prevent them from being deleted.
 * See {@link UsedOptions}
 */
@NoBootstrap
@Mixin(targets = {"net/minecraft/client/Options$2"})
public class OptionUseTracker
{
    @Inject(
            method = "getValueOrNull",
            at = @At(
                    value = "INVOKE",
                    target = "Lnet/minecraft/nbt/CompoundTag;contains(Ljava/lang/String;)Z"
            )
    )
    private void removeValueFromCompoundTag(String pKey, CallbackInfoReturnable<String> cir)
    {
        UsedOptions.usedOptions.add(pKey);
    }
}
#endif