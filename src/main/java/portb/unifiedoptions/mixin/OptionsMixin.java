/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.mixin;

#if !(MC12 || MC16)

#if MC19_2 || MC19_3
import net.minecraft.client.OptionInstance;
import java.util.function.Consumer;
import com.google.gson.JsonElement;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.JsonOps;
#else
import java.util.function.IntFunction;
import java.util.function.ToIntFunction;
#endif

#if MC17
import net.minecraftforge.fmlclient.ClientModLoader;
#else
import net.minecraftforge.client.loading.ClientModLoader;
#endif

import manifold.ext.rt.api.auto;
import manifold.rt.api.NoBootstrap;
import net.minecraft.SharedConstants;
import net.minecraft.client.Options;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.StringTag;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import portb.unifiedoptions.Config;
import portb.unifiedoptions.FileUtil;
import portb.unifiedoptions.post17.UsedOptions;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

@NoBootstrap
@Mixin(Options.class)
public class OptionsMixin
{
    private static final String LOAD_METHOD = #if MC19_2 || MC19_3 "load(Z)V" #else "load()V" #endif;
    private static final boolean LOAD_REMAP = #if MC19_2 || MC19_3 false #else true #endif;
    
    private static String unusedOptions = "";
    
    /**
     * Forces the game to always process options even if options.txt doesn't exist
     **/
    @Redirect(
            method = LOAD_METHOD,
            at = @At(
                    value = "INVOKE",
                    target = "Ljava/io/File;exists()Z"
            ),
            remap = false
    )
    boolean alwaysReadOptionsFile(File file)
    {
        return true;
    }
    
    @Redirect(
            method = LOAD_METHOD,
            at = @At(
                    value = "INVOKE",
                    target = "Lcom/google/common/io/Files;newReader(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/BufferedReader;",
                    remap = false
            ),
            remap = LOAD_REMAP
    )
    private BufferedReader aggregateOptionsFilesIntoLinesList(File file, Charset charset)
    {
        List<String> lines = FileUtil.readAllLinesOrEmptyList(file.toPath());
        
        //this is needed by dataFix() or an error will occur
        lines.add("version:" + SharedConstants.getCurrentVersion().getWorldVersion());
        
        aggregateLines(lines,
                       FileUtil.readAllLinesOrEmptyList(Config.universalOptionsFile),
                       Config.blacklistedUniversalOptions
        );
        
        return new BufferedReader(Reader.nullReader())
        {
            @Override
            public Stream<String> lines()
            {
                return lines.stream();
            }
            
            @Override
            public void close()
            {
            
            }
        };
    }
    
    private void aggregateLines(List<String> destination, List<String> linesFromFile, List<String> undesiredOptions)
    {
        linesFromFile.forEach(line -> {
            auto split = line.split(":", 2);
            
            if (split.length == 2)
            {
                if (!undesiredOptions.contains(split[0]))
                    destination.add(line);
            }
        });
    }
    
    @Inject(
            method = LOAD_METHOD,
            at = @At(
                    value = "INVOKE",
                    #if MC19_2 || MC19_3
                    target = "Ljava/util/function/Consumer;accept(Ljava/lang/Object;)V",
                    #else
                    target = "Lnet/minecraft/client/Options;processOptions(Lnet/minecraft/client/Options$FieldAccess;)V",
                    #endif
                    shift = At.Shift.AFTER
            ),
            locals = LocalCapture.CAPTURE_FAILHARD,
            remap = LOAD_REMAP
    )
    #if MC19_2
    private void trackUnusedOptions(boolean limited, CallbackInfo ci, CompoundTag _unused, BufferedReader bufferedreader, CompoundTag compoundTag, Consumer<?> processor)
    #elif MC19_3
    private void trackUnusedOptions(boolean limited, CallbackInfo ci, CompoundTag _unused, CompoundTag compoundTag, Consumer<?> processor)
    #else
    private void trackUnusedOptions(CallbackInfo ci, CompoundTag _unused, BufferedReader bufferedreader, CompoundTag compoundTag)
    #endif
    {
        StringBuilder builder = new StringBuilder();
        compoundTag.remove("version");
    
        for (var tag : compoundTag.tags.entrySet())
        {
            if (!UsedOptions.usedOptions.contains(tag.key) && !Config.blacklistedUniversalOptions.contains(tag.key))
            {
                builder.append(tag.key).append(":");
                
                if (tag.value instanceof StringTag stringTag)
                    builder.append(stringTag.asString);
                else
                    builder.append(tag.value);
                
                builder.append("\n");
            }
        }
        
        unusedOptions = builder.toString();
    }
    
    /**
     * @author PORTB
     * @reason Rationale for @Overwrite: can't think of any other ways to ensure that the extra PrintStreams are closed if an exception occurs
     */
    @Overwrite
    @SuppressWarnings({"NullableProblems", "resource"})

    public void save()
    {
        Options self = (Options) (Object) this;
    
        if (ClientModLoader.isLoading())
            return; //Don't save settings before mods add keybindings and the like to prevent them from being deleted.
    
        try (
                final PrintWriter perInstance = new PrintWriter(new OutputStreamWriter(new FileOutputStream(self.optionsFile),
                                                                                       StandardCharsets.UTF_8
                ));
                final PrintWriter universal = Config.universalOptionsPrintWriter
        )
        {
            self.processOptions(new Options.FieldAccess()
            {
                
                private PrintWriter writer(String name)
                {
                    if (!Config.blacklistedUniversalOptions.contains(name))
                        return universal;
                    else
                        return perInstance;
                }
                
                public void writePrefix(String name)
                {
                    var writer = writer(name);
                    
                    writer.print(name);
                    writer.print(':');
                }
    
                #if MC19_2 || MC19_3
                public <T> void process(String pName, OptionInstance<T> pOptionInstance)
                {
                    DataResult<JsonElement> dataresult = pOptionInstance.codec().encodeStart(JsonOps.INSTANCE,
                                                                                             pOptionInstance.get()
                    );
                    dataresult.error().ifPresent((p_232133_) -> {
                        Options.LOGGER.error("Error saving option ${pOptionInstance}: ${p_232133_}");
                    });
                    dataresult.result().ifPresent((element) -> {
                        this.writePrefix(pName);
                        
                        //fixes some option values being quoted for some reason
                        String        str     = Options.GSON.toJson(element);
                        StringBuilder builder = new StringBuilder(str);
                        
                        if (str.startsWith("\""))
                            builder.delete(0, 1);
                        
                        if (str.endsWith("\""))
                            builder.delete(builder.length() - 1, builder.length());
                        
                        writer(pName).println(builder);
                    });
                }
                #else
                public double process(String pName, double pValue)
                {
                    this.writePrefix(pName);
                    writer(pName).println(pValue);
                    return pValue;
                }
    
                public <T> T process(String pName, T pValue, IntFunction<T> pIntValuefier, ToIntFunction<T> pValueIntifier)
                {
                    this.writePrefix(pName);
                    writer(pName).println(pValueIntifier.applyAsInt(pValue));
                    return pValue;
                }
                #endif
    
                public int process(String pName, int pValue)
                {
                    this.writePrefix(pName);
                    writer(pName).println(pValue);
                    return pValue;
                }
    
                public boolean process(String pName, boolean pValue)
                {
                    this.writePrefix(pName);
                    writer(pName).println(pValue);
                    return pValue;
                }
                
                public String process(String pName, String pValue)
                {
                    this.writePrefix(pName);
                    writer(pName).println(pValue);
                    return pValue;
                }
                
                public float process(String pName, float pValue)
                {
                    this.writePrefix(pName);
                    writer(pName).println(pValue);
                    return pValue;
                }
                
                public <T> T process(String pName, T pValue, Function<String, T> pStringValuefier, Function<T, String> pValueStringifier)
                {
                    this.writePrefix(pName);
                    writer(pName).println(pValueStringifier.apply(pValue));
                    return pValue;
                }
            });
            
            if (self.minecraft.window.preferredFullscreenVideoMode.isPresent())
            {
                perInstance.println("fullscreenResolution: ${self.minecraft.window.preferredFullscreenVideoMode.get().write()}");
            }
            
            //preserve unknown options
            universal.print(unusedOptions);
        }
        catch (Exception exception)
        {
            Options.LOGGER.error("Failed to save options", exception);
        }
        
        self.broadcastOptions();
    }
}

#endif