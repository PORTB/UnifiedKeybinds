/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.mixin;

import manifold.ext.rt.api.auto;
import manifold.rt.api.NoBootstrap;
import net.minecraft.client.main.Main;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import portb.unifiedoptions.Config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

#if MC18 || MC19_2 || MC19_3
import com.mojang.logging.LogUtils;
#else
import org.apache.logging.log4j.LogManager;
#endif

#if MC16
import net.minecraft.util.SharedConstants;
#elif !MC12
import net.minecraft.SharedConstants;
#endif

@NoBootstrap
@Mixin(Main.class)
public abstract class DefaultConfigsCopier
{
    @Inject(
           method = #if MC19_3 || MC19_2 "run" #else "main" #endif,
           at = @At(
                   value = "INVOKE",
                   #if MC16
                   target = "Lnet/minecraft/client/Minecraft;<init>(Lnet/minecraft/client/GameConfiguration;)V"
                   #elif MC12
                   target = "Lnet/minecraft/client/Minecraft;<init>(Lnet/minecraft/client/main/GameConfiguration;)V"
                   #else
                   target = "Lnet/minecraft/client/Minecraft;<init>(Lnet/minecraft/client/main/GameConfig;)V"
                   #endif
           )
    )
    private static void copyDefaultOptions(CallbackInfo ci)
    {
        auto logger = #if MC17 || MC16 || MC12 LogManager.getLogger() #else LogUtils.getLogger() #endif;
        
        try
        {
            Path extraFilesDirectory = Config.globalConfigDirectory.resolve(getMinecraftVersionName()).resolve("extras");
    
            Files.createDirectories(extraFilesDirectory);
    
            //noinspection resource
            Files.walk(extraFilesDirectory).forEach( file -> {
                //don't copy the root file
                if(extraFilesDirectory.equals(file))
                    return;
                
                Path destination = extraFilesDirectory.relativize(file);
                
                if((Config.canOverwriteExistingConfigs || !Files.exists(destination)) && !Files.isDirectory(file))
                {
                    try
                    {
                        Files.copy(file, destination, StandardCopyOption.REPLACE_EXISTING);
                    }
                    catch (IOException e)
                    {
                        logger.error("UnifiedKeybinds: FAILED TO COPY ${file.fileName} -> ${destination.fileName}", e);
                    }
                }
            });
        }
        catch (IOException e)
        {
            logger.error("UnifiedKeybinds: Could not copy default options", e);
        }
    }
    
    private static String getMinecraftVersionName()
    {
        #if MC12
        return "1.12";
        #elif MC16 || MC17 || MC18
        String[] split = SharedConstants.currentVersion.name.split("\\.", 3);
        return "${split[0]}.${split[1]}";
        #else
        //starting from 1.19.3, minor versions may be completely different from each other
        return SharedConstants.currentVersion.name;
        #endif
    }
}
