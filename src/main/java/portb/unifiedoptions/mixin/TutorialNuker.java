/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions.mixin;

import manifold.rt.api.NoBootstrap;
import net.minecraft.client.tutorial.TutorialSteps;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

#if MC16
import net.minecraft.client.GameSettings;
#elif MC12
import net.minecraft.client.settings.GameSettings;
#else
import net.minecraft.client.Options;
#endif

/**
 * Disables vanilla's stupid tutorial by changing the default option value to completed
 */
@NoBootstrap
@Mixin(#if MC16 || MC12 GameSettings.class #else Options.class #endif )
public class TutorialNuker
{
    #if !(MC12 || MC16)
    @Redirect(
            method = "<init>",
            at = @At(
                    value = "FIELD",
                    target = "Lnet/minecraft/client/Options;tutorialStep:Lnet/minecraft/client/tutorial/TutorialSteps;",
                    opcode = Opcodes.PUTFIELD
            )
    )
    private void completeTutorialByDefault(Options instance, TutorialSteps value)
    {
        instance.tutorialStep = TutorialSteps.NONE;
    }
    #else
    @Redirect(
            method = "<init>*",
            at = @At(
                    value = "FIELD",
                    #if MC16
                    target = "Lnet/minecraft/client/GameSettings;tutorialStep:Lnet/minecraft/client/tutorial/TutorialSteps;",
                    #elif MC12
                    target = "Lnet/minecraft/client/settings/GameSettings;tutorialStep:Lnet/minecraft/client/tutorial/TutorialSteps;",
                    #endif
                    opcode = Opcodes.PUTFIELD
            )
    )
    private void completeTutorialByDefault(GameSettings instance, TutorialSteps value)
    {
        instance.tutorialStep = TutorialSteps.NONE;
    }
    #endif
}
