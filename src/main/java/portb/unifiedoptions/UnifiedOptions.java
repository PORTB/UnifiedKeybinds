/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions;

import manifold.rt.api.NoBootstrap;

import net.minecraftforge.fml.common.Mod;

#if !MC12
import net.minecraftforge.fml.ModLoadingContext;
#endif

#if MC16
import net.minecraftforge.fml.ExtensionPoint;
import org.apache.commons.lang3.tuple.Pair;
#elif !MC12
import net.minecraftforge.fml.IExtensionPoint;
#endif

@NoBootstrap
#if MC12
@Mod(modid = UnifiedOptions.MOD_ID, name = "UnifiedKeybinds", version = "1.1", clientSideOnly = true, acceptableRemoteVersions = "*")
#else
@Mod(UnifiedOptions.MOD_ID)
#endif
public class UnifiedOptions
{
    public static final String MOD_ID = "unifiedoptions";
    
    public UnifiedOptions()
    {
        #if MC16
        ModLoadingContext.get().registerExtensionPoint(ExtensionPoint.DISPLAYTEST, () -> Pair.of(() -> net.minecraftforge.fml.network.FMLNetworkConstants.IGNORESERVERONLY, (s, v) -> true));
        #elif MC17
        ModLoadingContext.get().registerExtensionPoint(IExtensionPoint.DisplayTest.class, () -> new IExtensionPoint.DisplayTest(() -> net.minecraftforge.fmllegacy.network.FMLNetworkConstants.IGNORESERVERONLY, (a, b) -> true));
        #elif !MC12
        ModLoadingContext.get().registerExtensionPoint(IExtensionPoint.DisplayTest.class, () -> new IExtensionPoint.DisplayTest(() -> net.minecraftforge.network.NetworkConstants.IGNORESERVERONLY, (a, b) -> true));
        #endif
    }
}
