/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions;

import com.electronwill.nightconfig.core.ConfigSpec;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.google.common.collect.Lists;
import manifold.rt.api.NoBootstrap;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@NoBootstrap
public class Config
{
    private static final String DIRECTORY_OPTION = "synchronization.directory";
    private static final String ENABLED_OPTION = "enabled";
    private static final String BLACKLIST_OPTION = "synchronization.blacklisted";
    private static final String UNSYNCHRONIZED_OPTION = "synchronization.blacklisted";
    private static final String OVERWRITE_OPTION = "default_config.allow_overwrite";
    
    private static final Path GLOBAL_CONFIG_DIRECTORY = Paths.get(System.getProperty("user.home"), "MinecraftOptions");
    private static final Path GLOBAL_CONFIG_FILE      = GLOBAL_CONFIG_DIRECTORY.resolve("config.toml");
    private static final Path INSTANCE_CONFIG_FILE    = Paths.get("universaloptions.toml");
    
    private final static List<String> blacklistedUniversalOptions;
    private final static Path universalOptionsFile;
    private final static boolean enabled;
    private final static boolean allowExtraConfigsOverwrite;
    
    static
    {
        try (
                CommentedFileConfig globalFile = CommentedFileConfig.of(GLOBAL_CONFIG_FILE);
                CommentedFileConfig instanceFile = CommentedFileConfig.of(INSTANCE_CONFIG_FILE)
        )
        {
            Path workingDirectory;
            
            globalFile.load();
            instanceFile.load();
            
            setupGlobalConfigComments(globalFile);
            setupInstanceConfigComments(instanceFile);
            
            if (globalFile.contains(DIRECTORY_OPTION))
                workingDirectory = Paths.get((String) globalFile.get(DIRECTORY_OPTION));
            else
                workingDirectory = GLOBAL_CONFIG_DIRECTORY;
    
            enabled = globalFile.get(ENABLED_OPTION);
            
            blacklistedUniversalOptions = globalFile.get(BLACKLIST_OPTION);
            universalOptionsFile = workingDirectory.resolve("universal.txt");
            
            blacklistedUniversalOptions.addAll(instanceFile.get(UNSYNCHRONIZED_OPTION));
            
            allowExtraConfigsOverwrite = globalFile.get(OVERWRITE_OPTION);
            
            //for some reason, in 1.12 this causes the game to hang just before startup but works without it anyway
            //but in 1.16 and above, the file does not get saved without this, but the game does not hang for some reason
            #if !MC12
            globalFile.save();
            instanceFile.save();
            #endif
        }
        catch (Throwable e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private static void setupGlobalConfigComments(CommentedFileConfig fileConfig)
    {
        ConfigSpec spec = new ConfigSpec();

        spec.define(ENABLED_OPTION, true);
        spec.define(DIRECTORY_OPTION, GLOBAL_CONFIG_DIRECTORY.toString());
        spec.defineList(BLACKLIST_OPTION,
                        Lists.newArrayList("tutorialStep",
                                           "overrideWidth",
                                           "overrideHeight",
                                           "resourcePacks",
                                           "incompatibleResourcePacks",
                                           "renderDistance",
                                           "simulationDistance",
                                           "fullscreen",
                                           "fullscreenResolution"
                        ),
                        it -> true
        );
        spec.define(OVERWRITE_OPTION, false);
        
        spec.correct(fileConfig);
    
        fileConfig.setComment(ENABLED_OPTION, "Enables or disables the mod");
        fileConfig.setComment(DIRECTORY_OPTION, "The directory that options should be stored in");
        fileConfig.setComment(BLACKLIST_OPTION,
                              "Options that should *NOT* be synchronized across all instances and all versions\n" +
                                      "See https://minecraft.fandom.com/wiki/Options.txt#Java_Edition for what they all do"
        );
        fileConfig.setComment(OVERWRITE_OPTION, "Allows files in <version>/extras to overwrite already existing files in your instance");
    }
    
    private static void setupInstanceConfigComments(CommentedFileConfig fileConfig)
    {
        ConfigSpec spec = new ConfigSpec();
    
        spec.defineList(UNSYNCHRONIZED_OPTION, new ArrayList<String>(), it -> true);
    
        spec.correct(fileConfig);
    
        fileConfig.setComment(UNSYNCHRONIZED_OPTION,
                              "Options that should not be synchronised with the universal or per-version configs.\n" +
                                      "Changes made to these config values for this instance will not have an effect anywhere else"
        );
    }

    //region crap accessors
    public static Path getGlobalConfigDirectory()
    {
        return GLOBAL_CONFIG_DIRECTORY;
    }
    
    public static Path getUniversalOptionsFile()
    {
        return universalOptionsFile;
    }

    public static boolean isEnabled()
    {
        return enabled;
    }
    public static boolean getCanOverwriteExistingConfigs() { return allowExtraConfigsOverwrite; }
    
    public static List<String> getBlacklistedUniversalOptions()
    {
        return blacklistedUniversalOptions;
    }
    
    public static PrintWriter getUniversalOptionsPrintWriter() throws IOException
    {
        return getPrintWriterForPath(universalOptionsFile);
    }
    
    private static PrintWriter getPrintWriterForPath(Path path) throws IOException
    {
        Files.createDirectories(path.parent);
        
        return new PrintWriter(new OutputStreamWriter(new FileOutputStream(path.toFile()), StandardCharsets.UTF_8));
    }
    //endregion
}
