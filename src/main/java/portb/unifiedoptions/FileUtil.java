/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.unifiedoptions;

import manifold.rt.api.NoBootstrap;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@NoBootstrap
public class FileUtil
{
    public static List<String> readAllLinesOrEmptyList(Path path)
    {
        if (!Files.exists(path))
        {
            return new ArrayList<>();
        }
        else
        {
            try
            {
                return Files.readAllLines(path, StandardCharsets.UTF_8);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
    }
}
